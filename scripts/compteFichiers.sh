#!/bin/bash

if test $# = 0
then
	echo "Vous devez passer des noms de répeertoires en paramètres"
else
	for arg in $*
	do
		if ! test -e $arg
		then
			echo "$arg n'existe pas"
		elif ! test -d $arg
		then
			echo "Le fichier $arg n'est pas un répertoire"
		else
			n=$(ls -l $arg|grep -E ^-|wc -l)
			m=$(ls -l $arg|grep -E ^d|wc -l)
			echo "Le répertoire $arg contient $n fichiers réguliers et $m répertoires"
		fi
	done
fi
